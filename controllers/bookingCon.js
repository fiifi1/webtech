const mongoose = require("mongoose");   //imports mongoose to use with mongodb
const bodyParser = require("body-parser");

mongoose.connect("mongodb://localhost/booking", { useNewUrlParser: true });

let bookingSchema = mongoose.Schema({
    flightID: String,
    passengerID: String,
    departureTime: String,
    destination: String,
    arrivaltime: Date
});

//let bookingModel = mongoose.model(bookingModel, flightSchema)
//the above line throws an errow 

let urlEncodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function (app) {
    app.get("/booking", (req, res) => {
        //Load some data from mongoDb and add res
        //flightsModel.find({}, (err, data) => {
        //  if (err) throw err;
        //res.status(200).send(__dirname + "/booking.html", { /*books: data*/ });
        //});
        res.status(200).send(__dirname + "/booking.html");
    });
}