const mongoose = require("mongoose");
const bodyParser = require("body-parser");

mongoose.connect("mongodb://localhost/flights", { useNewUrlParser: true });

let flightSchema = mongoose.Schema({
    flightID: String,
    origin: String,
    departureTime: String,
    destination: String,
    arrivaltime: Date
});

//let flightsModel = mongoose.model(flightsModel, flightSchema)
//the above line throws an errow 

let urlEncodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function (app) {
    app.get("/flights", (req, res) => {
        //Load some data from mongoDb and add res
        //flightsModel.find({}, (err, data) => {
        //  if (err) throw err;
        //res.status(200).send(__dirname + "/flights.html", { /*flights: data*/ }); //Data that is loaded from mongoDB would be accessible by the DOM
        //});
        res.status(200).send(__dirname + "/flights.html");
    });

}