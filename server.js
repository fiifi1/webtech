"use strict";

const express = require("express");
const index = require("./controllers/indexCon.js");
const about = require("./controllers/aboutCon.js");
const contact = require("./controllers/contactCon.js");
const flights = require("./controllers/flightsCon.js");
const booking = require("./controllers/bookingCon.js");


function startServer() {
    const app = express();

    // Handle requests for static files
    app.use(express.static("public"));

    //fire controllers
    index(app);
    about(app);
    contact(app);
    flights(app);
    booking(app);

    // Start the server
    const PORT = process.env.PORT || 3000;
    return app.listen(PORT, () => {
        // eslint-disable-next-line no-console
        console.log("Local Server Started on port 3000...");
    });
}

startServer();
